import { useState } from 'react';
import TodoList from './components/todo-list';
import NewItem from './components/new-item';

// A to-do list to use for testing purposes
const initialTodos = [
  { description: 'Finish lecture', isComplete: false },
  { description: 'Do homework', isComplete: false },
  { description: 'Sleep', isComplete: false }
];

function App() {

  const [todos, setTodos] = useState(initialTodos);

  const handleChange = (index) => {
    // in React - mutating an object is when you change values inside the object, but the object reference does not change
    // 'todos" gets changed, but the reference value doesn't, so React does not register this as a change to the object
    // the ToDoList component will not get rerendered, and we will not see the checkbox state change
    // this is why we need to create a new array, spread the original values into it, and change the checked value
    const updatedTodos = [...todos];
    updatedTodos[index].isComplete = !updatedTodos[index].isComplete;
    setTodos(updatedTodos);
  }

  const handleAdd = (description) => setTodos([...todos, {description, isComplete: false}]);

  const handleRemove = (index) => {
    todos.splice(index, 1);
    setTodos([...todos]);
  }

  return (
    <div> 
      <div>
        <h1>My todos</h1>
        {todos && todos.length ? <TodoList items={todos} onChange={handleChange} onRemove={handleRemove}/> : <span>There are no to-do items!</span>}
      </div>

      <div>
        <h1>Add item</h1>
        <NewItem onAdd={handleAdd}/>
      </div>

    </div>
  );
}

export default App;