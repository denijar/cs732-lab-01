import React from 'react';
import './styles.css';

const TodoList = ({ items, onChange, onRemove }) => {
    return (
        <div className={"itemList"}>
            {items.map((item, index) => (
                <div key={index} className={`item ${item.isComplete ? "checked" : "unchecked"} key={index}`}>
                    <label>
                        <input type="checkbox" checked={item.isComplete} onChange={() => onChange(index)}/>
                        {item.description}
                        {item.isComplete && <span> (Done!)</span>}
                    </label>
                    <button onClick={() => onRemove(index)}>Remove</button>
                </div>
            ))}
        </div>          
    )
};

export default TodoList;