import React from 'react';

const NewItem = ({ onAdd }) => {
    return (
        <div>
            <label>
                Description: 
                <input id="inputBox" type="text"/>
            </label>
            <button onClick={() => onAdd(document.getElementById('inputBox').value)}>Add</button>
            
        </div>          
    )
};

export default NewItem;